package graphtutorial

import com.microsoft.aad.msal4j.DeviceCode
import com.microsoft.aad.msal4j.DeviceCodeFlowParameters
import com.microsoft.aad.msal4j.PublicClientApplication
import java.net.MalformedURLException
import java.util.concurrent.Executors

/**
 * Authentication
 */
class Authentication {
    companion object {
        private var applicationID: String? = null
        // Set authority to allow only organisational accounts
        // Device code flow only supports organisational accounts
        private const val authority = "https://login.microsoftonline.com/common/"

        @JvmStatic
        fun initialise(applicationID: String) {
            Authentication.applicationID = applicationID
        }

        @JvmStatic
        fun getUserAccessToken(scopes: Array<String>): String? {
            if (applicationID == null) {
                println("You must initialise Authentication before calling getUserAccessToken")
                return null;
            }

            val scopeSet = scopes.toSet()

            val pool = Executors.newFixedThreadPool(1)
            val app: PublicClientApplication
            try {
                app = PublicClientApplication.builder(applicationID)
                    .authority(authority)
                    .executorService(pool)
                    .build()
            } catch (exception: MalformedURLException) {
                println("problem")
                return null
            }

            // Create consumer to receive the DeviceCode object
            // This method gets executed during the flow and provides
            // the URL the user logs into and the device code to enter
            val deviceCodeConsumer = { deviceCode: DeviceCode ->
                // Print the login information to the console
                println(deviceCode.message())
            }

            val result = app.acquireToken(
                DeviceCodeFlowParameters
                    .builder(scopeSet, deviceCodeConsumer)
                    .build()
            ).exceptionally {
                println("Unable to authenticate - $it")
                return@exceptionally null
            }.join()

            pool.shutdown()

            if (result != null) {
                return result.accessToken()
            }

            return null
        }
    }
}
