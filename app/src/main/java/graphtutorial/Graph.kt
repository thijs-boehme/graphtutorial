package graphtutorial

import com.microsoft.graph.logger.DefaultLogger
import com.microsoft.graph.logger.LoggerLevel
import com.microsoft.graph.models.extensions.Event
import com.microsoft.graph.models.extensions.IGraphServiceClient
import com.microsoft.graph.models.extensions.User
import com.microsoft.graph.options.Option
import com.microsoft.graph.options.QueryOption
import com.microsoft.graph.requests.extensions.GraphServiceClient
import java.util.*

class Graph {
    companion object {
        private var graphClient: IGraphServiceClient? = null
        private var authProvider: SimpleAuthProvider? = null

        fun ensureGraphClient(accessToken: String) {
            if (graphClient == null) {
                // Create the auth provider
                authProvider = SimpleAuthProvider(accessToken)

                // Create default logger to only log errors
                val logger = DefaultLogger()
                logger.loggingLevel = LoggerLevel.ERROR

                // Build a Graph client
                graphClient = GraphServiceClient.builder()
                    .authenticationProvider(authProvider)
                    .logger(logger)
                    .buildClient()
            }
        }

        @JvmStatic
        fun getUser(accessToken: String): User {
            ensureGraphClient(accessToken)

            // GET /me to get authenticated user
            return graphClient!!.me()
                .buildRequest()
                .get()
        }

        @JvmStatic
        fun getEvents(accessToken: String): List<Event> {
            ensureGraphClient(accessToken)

            // Use QueryOption to specify the $orderby query parameter
            val options: MutableList<Option> = LinkedList<Option>()
            // Sort results by createdDateTime, get newest first
            options.add(QueryOption("orderby", "createdDateTime DESC"))

            // GET /me/events
            return graphClient!!.me()
                .events()
                .buildRequest(options)
                .select("subject,organizer,start,end")
                .get()
                .currentPage
        }
    }
}
